Download the ProQ2 repository:
git clone https://github.com/bjornwallner/ProQ_scripts.git

Install Rosetta and set all required paths according to instructions in ProQ_scripts

Run these scripts in order:
./compute_profiles.sh
./compute_scores.py
./collect_scores.sh

