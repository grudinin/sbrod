#!/usr/bin/bash


if [ $# -ne 1 ]; then
  echo "Number of provided input arguments: $#"
  echo "Usage: $0 <pdb_structure>"
  exit 1
fi

x=$1


# score the protein structure
score.static.linuxgccrelease -database $ROSETTA_PATH/database -in:file:fullatom \
    -score:weights ProQ2 \
    -ProQ:basename $x \
    -in:file:s $x \
    -ignore_zero_occupancy false \
    -out:file:scorefile $x.ProQ2


# ProQ2-refine

# repack side-chains
relax.linuxgccrelease -database $ROSETTA_PATH/database -in:file:fullatom \
    -nstruct 10 \
    -relax:script ProQ_scripts/resampling/repack.script \
    -in:file:s $x \
    -ignore_zero_occupancy false \
    -out:file:silent_struct_type binary \
    -out:file:silent $x.temp_repacked

# score repacked structures
score.linuxgccrelease -database $ROSETTA_PATH/database -in:file:fullatom \
    -score:weights ProQ2 \
    -ProQ:basename $x \
    -in:file:silent $x.temp_repacked \
    -out:file:scorefile $x.ProQ2repacked

# remove temporary dumps
rm $x.temp_repacked
