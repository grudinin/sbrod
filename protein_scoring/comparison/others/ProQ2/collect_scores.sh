#!/usr/bin/bash


scores_filename="CASP12_ProQ2.txt"
refine_scores_filename="CASP12_ProQ2refine.txt"


for x in $(cat list_stage_1.txt) $(cat list_stage_2.txt)
do
    if [ ! -f $x.ProQ2 ]; then
        echo "ERROR: $x.ProQ2 does not exist"
        continue
    fi

    # initialize files with scores
    if [ ! -f $scores_filename ]; then
        printf "%s    File\n" "$(head -1 $x.ProQ2)" > $scores_filename
    fi

    # pick the highest score
    scores=$(cat $x.ProQ2 | tail -1)
    printf "%s    %s\n" "$scores" $x >> $scores_filename


    if [ ! -f $x.ProQ2repacked ]; then
        echo "ERROR: $x.ProQ2repacked does not exist"
        continue
    fi

    # initialize files with scores
    if [ ! -f $refine_scores_filename ]; then
        printf "%s    File\n" "$(head -1 $x.ProQ2repacked)" > $refine_scores_filename
    fi

    # pick the highest score
    scores=$(cat $x.ProQ2repacked | tail -n +2 | grep SCORE: | sort -n | tail -1)
    printf "%s    %s\n" "$scores" $x >> $refine_scores_filename

done
