#!/usr/bin/env bash


QA_targets=~/Downloads/QA_*/*

declare -a teams=(
    "338" # ProQ2
    "132" # ProQ2-refine
    "363" # Wang_SVM
    "037" # VoroMQA
    "410" # Pcons-net
    "239" # ModFOLDclust2
    "347" # Wallner
    "478" # RFMQA
    "216" # myprotein-me
)

for target in $QA_targets; do
    for team in "${teams[@]}"; do
        echo $team ${teams[$team]}
        submission_stage1="${target}/$(basename $target)QA${team}_1"
        submission_stage2="${target}/$(basename $target)QA${team}_2"
        cat $submission_stage2
    done

    exit 1
done
