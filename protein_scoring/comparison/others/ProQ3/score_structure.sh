#!/usr/bin/bash


if [ $# -ne 1 ]; then
  echo "Number of provided input arguments: $#"
  echo "Usage: $0 <pdb_structure>"
  exit 1
fi

x=$1

rm $x.*proq2
rm $x.*proq3
rm $x.*lowres
rm $x.*highres
rm $x.*sscore.local
rm $x.*rosetta.log
rm $x*.svm
rm $x*.repacked*
rm $x.pdb.*
rm $x.proq3.*

# ProQ3-repacked
./proq3/run_proq3.sh $x -deep no -keep_files yes -repack yes

mv $x.pdb.proq3.sscore.global $x.ProQ3repacked
mv $x.proq3.sscore.global $x.ProQ3repacked

rm $x.*proq2
rm $x.*proq3
rm $x.*lowres
rm $x.*highres
rm $x.*sscore.local
rm $x.*rosetta.log
rm $x*.svm
rm $x*.repacked*
rm $x.pdb.*

# ProQ3
./proq3/run_proq3.sh $x -deep no -keep_files yes -repack no

mv $x.pdb.proq3.sscore.global $x.ProQ3
mv $x.proq3.sscore.global $x.ProQ3

rm $x.*proq2
rm $x.*proq3
rm $x.*lowres
rm $x.*highres
rm $x.*sscore.local
rm $x.*rosetta.log
rm $x*.svm
rm $x*.repacked*
rm $x.pdb.*
